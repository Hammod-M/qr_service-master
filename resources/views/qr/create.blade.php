@extends('adminlte::page')

@section('title', __('QR Card Create'))

@section('content_header')
    <h1>{{ __('QR Card Create') }}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('QR Card Create') }}</div>

                    <div class="card-body">
                        @if(session('success'))
                            <x-adminlte-alert theme="success" title="{{ __('Success') }}" dismissable>
                                {{ session('success') }}
                            </x-adminlte-alert>
                        @endif
                        <form action="{{ route('qr.store', $qr->id) }}" method="post" class="container">
                            @csrf
                            @method('PUT')
                            @include('qr.form')
                            <x-adminlte-button class="btn" type="submit" label="{{ __('Save') }}" theme="success"></x-adminlte-button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
