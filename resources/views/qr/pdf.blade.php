<html>
<head>
    <title>{{ $qr->title }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @page {
            margin: 10px;
        }

        body {
            font-family: 'DejaVu Sans', sans-serif;
            font-size: 12pt;
            margin: 0;
        }

        table {
            table-layout: auto;
            width: 100%;
        }

        td {
            padding: 10px;
        }

        .qr_card {
            text-align: center;
            width: 100%;
        }

        .leading_text {
            font-size: 1.15em;
        }

        .qr {
            width: 250px;
        }

        .small {
            font-size: 0.85em;
        }

        .small .qr {
            width: 200px;
        }
    </style>
</head>
<body>
<table style="page-break-after: always;">
    @for ($j = 0; $j < 2; $j++)
        <col style="width:50%">
    @endfor
    @for ($i = 0; $i < 3; $i++)
        <tr>
            @for ($j = 0; $j < 2; $j++)
                <td>
                    <div class="qr_card">
                        @if ($qr->parameters['leading_text'] ?? null)
                            <div class="leading_text">{{ $qr->parameters['leading_text'] }}</div>
                        @endif
                        <img class="qr" src="{{ $qrImage }}">
                        @foreach($qr->fields as $key => $value)
                            <div class="field"><strong>{{ $key }}:</strong> {{ $value }}</div>
                        @endforeach
                    </div>
                </td>
            @endfor
        </tr>
    @endfor
</table>
<table class="small">
    @for ($j = 0; $j < 3; $j++)
        <col style="width:33.3%">
    @endfor
    @for ($i = 0; $i < 4; $i++)
        <tr>
            @for ($j = 0; $j < 3; $j++)
                <td>
                    <div class="qr_card">
                        @if ($qr->parameters['leading_text'] ?? null)
                            <div class="leading_text">{{ $qr->parameters['leading_text'] }}</div>
                        @endif
                        <img class="qr" src="{{ $qrImage }}">
                        @foreach($qr->fields as $key => $value)
                            <div class="field"><strong>{{ $key }}:</strong> {{ $value }}</div>
                        @endforeach
                    </div>
                </td>
            @endfor
        </tr>
    @endfor
</table>
</body>
</html>
