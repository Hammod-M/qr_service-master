@section('plugins.Summernote', true)
@section('plugins.BsCustomFileInput', true)
@section('plugins.BootstrapSwitch', true)
@section('plugins.JqueryUI', true)

<div class="row">
    <x-adminlte-input name="name"
                      label="{{ __('Name') }}"
                      fgroup-class="col-md-12"
                      value="{{ $qr->name }}"
                      enable-old-support>
        <x-slot name="bottomSlot">
            <span class="text-sm text-gray">
                {{ __('Internal name') }}
            </span>
        </x-slot>
    </x-adminlte-input>
</div>

<div class="row">
    <x-adminlte-input name="title"
                      label="{{ __('Title') }}"
                      fgroup-class="col-md-12"
                      value="{{ $qr->title }}"
                      enable-old-support>
        <x-slot name="bottomSlot">
            <span class="text-sm text-gray">
                {{ __('Title for the public page') }}
            </span>
        </x-slot>
    </x-adminlte-input>
</div>

<div class="row">
    <x-adminlte-input name="label"
                      label="{{ __('Event label') }}"
                      fgroup-class="col-md-12"
                      value="{{ $qr->label }}"
                      enable-old-support>
        <x-slot name="bottomSlot">
            <span class="text-sm text-gray">
                {{ __('To distinguish events by this field') }}
            </span>
        </x-slot>
    </x-adminlte-input>
</div>

<div class="row">
    <label class="col-md-12">{{ __('User settings') }}</label>
</div>

<div class="row ml-1 pl-2 border-left">
    <x-adminlte-input name="parameters__leading_text"
                      label="{{ __('Leading text') }}"
                      fgroup-class="col-md-9"
                      value="{{ $qr->parameters['leading_text'] ?? null }}"
                      enable-old-support readonly disabled>
        @if(($qr->parameters['leading_text'] ?? null) != ($qr->author->settings->parameters['leading_text'] ?? null))
            <x-slot name="bottomSlot">
                <span class="text-sm text-danger">
                    {{ __('Current and probably not relevant value') }}
                </span>
            </x-slot>
        @endif
    </x-adminlte-input>
    <x-adminlte-input-switch name="use_from_settings[leading_text]"
                             id="use_from_settings_leading_text"
                             label="{{ __('Update from settings') }}"
                             fgroup-class="col-md-3"
                             data-inverse="true"
                             enable-old-support>
    </x-adminlte-input-switch>

    <div class="col-md-9">
        <div class="container row d-flex flex-row align-items-center">
            <label>{{ __('Current logo:') }}</label>
            @if($qr->parameters['logo_url'] ?? [])
                <img class="ml-2" src="{{ asset($qr->parameters['logo_url']) }}" alt="Logo">
            @else
                <span class="ml-2 mb-2">{{ __('No logo') }}</span>
            @endif
        </div>
    </div>
    <x-adminlte-input-switch name="use_from_settings[logo_url]"
                             id="use_from_settings_logo_url"
                             label="{{ __('Update from settings') }}"
                             fgroup-class="col-md-3"
                             data-inverse="true"
                             enable-old-support>
    </x-adminlte-input-switch>
</div>

<div class="row">
    <label class="col-md-12">{{ __('Extra fields') }}</label>
</div>

<div class="row ml-1 pl-2 mb-2 border-left">
    @if (!$qr->fields && !$qr->author->settings->fields)
        <span class="ml-2 mb-2">{{ __('No extra fields added') }}</span>
    @else
        <ul id="sortable" class="col-6">
            @foreach($qr->fields as $field => $value)
                <x-adminlte-input name="parameters[fields][{{ $field }}]"
                                  label="{{ $field }}"
                                  value="{{ $value }}"
                                  enable-old-support>
                    <x-slot name="prependSlot">
                        <div class="input-group-text">
                            <i class="fas fa-user-cog"></i>
                        </div>
                    </x-slot>
                    <x-slot name="appendSlot">
                        <div class="input-group-text drag-n-drop">
                            <i class="fas fa-arrows-alt-v"></i>
                        </div>
                    </x-slot>
                    @if(!in_array($field, $qr->author->settings->fields))
                        <x-slot name="bottomSlot">
                            <span class="text-sm text-gray">
                                {{ __('Old field, leave empty to remove') }}
                            </span>
                        </x-slot>
                    @endif
                </x-adminlte-input>

            @endforeach
            @foreach($qr->author->settings->fields as $field)
                @if(!in_array($field, array_keys($qr->fields)))
                    <x-adminlte-input name="parameters[fields][{{ $field }}]"
                                      label="{{ $field }}"
                                      enable-old-support>
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user-cog"></i>
                            </div>
                        </x-slot>
                        <x-slot name="appendSlot">
                            <div class="input-group-text drag-n-drop">
                                <i class="fas fa-arrows-alt-v"></i>
                            </div>
                        </x-slot>
                    </x-adminlte-input>
                @endif
            @endforeach
        </ul>
    @endif
</div>

<div class="row">
    <label class="col-md-12">{{ __('Files') }}</label>
</div>

<div class="row ml-1 pl-2 mb-2 border-left">
    @php
        if ($errors->has('file.*')) {
            $errors->add('file', $errors->first('file.*'));
        }
    @endphp
    <x-adminlte-input-file id="ifFile"
                           label="{{ __('Add files to the page') }}"
                           name="file[]"
                           fgroup-class="col-md-12"
                           placeholder="{{ __('Add files...') }}"
                           multiple>
        <x-slot name="bottomSlot">
            <span class="text-sm text-gray">
                {{
                    trans_choice(
                        'Max :max files, each file max :max_mb MB, multiple choice available',
                        App\Http\Requests\QrCardUpdateRequest::MAX_FILES_COUNT, [
                            'max' => App\Http\Requests\QrCardUpdateRequest::MAX_FILES_COUNT,
                            'max_mb' => App\Http\Requests\QrCardUpdateRequest::MAX_FILES_SIZE_MB,
                        ]
                    )
                }}
            </span>
        </x-slot>
    </x-adminlte-input-file>
    @forelse($qr->file_links ?? [] as $file)
        @php
            $isDeleted = in_array(intval($file['id']), array_values(old('delete_files') ?? []));
        @endphp
        <div class="col-md-12 d-inline-flex flex-row align-items-center mb-2">
            <span id="fileName_{{ $file['id'] }}" @class(['file_link_admin', 'text-muted' => $isDeleted])>{{ $file['name'] }}</span>
            <a @class(['btn', 'btn-sm', 'btn-primary', 'mx-2', 'text-nowrap', 'disabled' => $isDeleted])
               href="{{ $file['url'] }}"
               download="{{ $file['name'] }}"
               id="fileDownloadBtn_{{ $file['id'] }}"
               target="_blank">
                <i class="fa fa-download"></i> {{ __('Download') }}
            </a>
            <x-adminlte-button type="button"
                               label="{{ __('Delete') }}"
                               class="btn-sm text-nowrap"
                               theme="danger"
                               icon="fas fa-trash"
                               id="fileDeleteBtn_{{ $file['id'] }}"
                               data-toggle="modal"
                               data-target="#modalDeleteFileConfirm_{{ $file['id'] }}"
                               :disabled="$isDeleted"
            ></x-adminlte-button>
            <input type="hidden"
                   name="delete_files[]"
                   id="deleteFile_{{ $file['id'] }}"
                   @if($isDeleted) value="{{ $file['id'] }}" @endif>
            <x-adminlte-modal id="modalDeleteFileConfirm_{{ $file['id'] }}" title="{{ __('Are you sure?') }}" theme="danger"
                              icon="fas fa-question-circle">
                {{ __('Are you sure you want to delete file :filename?', ['filename' => $file['name']]) }}
                <x-slot name="footerSlot">
                    <x-adminlte-button theme="danger" label="{{ __('Delete') }}" type="submit" data-dismiss="modal"
                                       data-file="{{ $file['id'] }}"
                                       onclick="deleteFile($(this).data('file'))">
                    </x-adminlte-button>
                    <x-adminlte-button label="{{ __('Close') }}" data-dismiss="modal"></x-adminlte-button>
                </x-slot>
            </x-adminlte-modal>
        </div>
    @empty
        <div class="col-md-12 d-inline-flex flex-row align-items-center mb-2">{{ __('No files uploaded') }}</div>
    @endforelse
</div>

@php
    $config = [
        'height' => 500,
        'disableDragAndDrop' => true,
        'toolbar' => [
            // [groupName, [list of button]]
            ['insert', ['link']]
        ]
    ];
@endphp
<div class="row">
    <x-adminlte-textarea name="body"
                            label="{{ __('Content') }}"
                            fgroup-class="col-md-12"
                            rows="20"
                            enable-old-support>{!! $qr->body !!}</x-adminlte-textarea>
</div>

<div class="row">
    @php
        $config = [
            'state' => $qr->published,
            'inverse' => true
        ]
    @endphp
    <x-adminlte-input-switch name="published"
                             label="{{ __('Publish') }}"
                             fgroup-class="col-md-12"
                             igroup-size="sm"
                             :config="$config"
                             :checked="$qr->published"
                             enable-old-support
                             >
        <x-slot name="bottomSlot">
            <span class="text-sm text-gray">
                {{ __('Your card will be published when checked') }}
            </span>
        </x-slot>
    </x-adminlte-input-switch>
</div>

@section('js')
    <script type="text/javascript">
        function deleteFile(key) {
            $(`input#deleteFile_${key}`).val(key)
            $(`#fileName_${key}`).addClass('text-muted')
            $(`#fileDownloadBtn_${key}`).addClass('disabled')
            $(`#fileDeleteBtn_${key}`).attr('disabled', 'disabled')
        }

        $(document).ready(function () {
            $('#sortable').sortable()
        })
    </script>
@endsection

@section('css')
    <style type="text/css">
        .file_link_admin {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .drag-n-drop {
            cursor: grab;
        }
    </style>
@stop
