@extends('adminlte::page')

@section('title', __('QR Card Edit'))

@section('content_header')
    <h1>{{ __('QR Card Edit') }}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('QR Card Edit') }}</div>

                    <div class="card-body">
                        @if(session('success'))
                            <x-adminlte-alert theme="success" title="{{ __('Success') }}" dismissable>
                                {{ session('success') }}
                            </x-adminlte-alert>
                        @endif
                        @if(session('warning'))
                            <x-adminlte-alert theme="warning" title="{{ __('Warning') }}" dismissable>
                                {{ session('warning') }}
                            </x-adminlte-alert>
                        @endif
                        <form id="editForm" action="{{ route('qr.update', $qr->id) }}" method="post" enctype="multipart/form-data" class="container">
                            @csrf
                            @method('PUT')
                            @include('qr.form')
                            <x-adminlte-button type="button"
                                               label="{{ __('Show') }}"
                                               theme="primary"
                                               data-toggle="modal"
                                               data-target="#modalCardPreview"></x-adminlte-button>
                            <x-adminlte-button type="submit"
                                               label="{{ __('Save') }}"
                                               form="editForm"
                                               theme="success"></x-adminlte-button>
                            <x-adminlte-button type="button"
                                               label="{{ __('Delete') }}"
                                               form="deleteForm"
                                               theme="danger"
                                               data-toggle="modal"
                                               data-target="#modalDeleteConfirm"></x-adminlte-button>
                        </form>
                        <form id="deleteForm" action="{{ route('qr.destroy', $qr->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                        </form>
                        <x-adminlte-modal id="modalDeleteConfirm" title="{{ __('Are you sure?') }}" theme="danger"
                                          icon="fas fa-question-circle">
                            {{ __('Are you sure you want to delete this QR?') }}
                            <x-slot name="footerSlot">
                                <x-adminlte-button theme="danger" label="{{ __('Delete') }}" type="submit" form="deleteForm"></x-adminlte-button>
                                <x-adminlte-button label="{{ __('Close') }}" data-dismiss="modal"></x-adminlte-button>
                            </x-slot>
                        </x-adminlte-modal>
                        <x-adminlte-modal id="modalCardPreview" title="{{ __('QR Card') }}" theme="default" size="lg">
                            <div class="d-flex flex-column align-items-center">
                                <div>{{ __('Here you can see your QR, short URL and download PDF') }}</div>
                                <div>
                                    <img src="{{ $qrImage ?? '' }}" alt="QR Code" />
                                </div>
                                <div>
                                    <a href="{{ route('qr.page', $qr->card_code, true) }}" target="_blank">{{ route('qr.page', $qr->card_code, true) }}
                                        <i class="fas fa-external-link-alt"></i>
                                    </a>
                                </div>
                                <div>
                                    <a class="btn btn-primary mt-2" href="{{ route('qr.pdf', $qr->id, true) }}" target="_blank">
                                        <i class="fa fa-download"></i> {{ $qr->title.'.pdf' }}
                                    </a>
                                </div>
                            </div>
                            <x-slot name="footerSlot">
                                <x-adminlte-button label="{{ __('Close') }}" data-dismiss="modal"></x-adminlte-button>
                            </x-slot>
                        </x-adminlte-modal>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
