@extends('adminlte::page')

@section('title', __('Catalog'))

@section('content_header')
    <h1>{{ __('Catalog') }}</h1>
@stop

@section('plugins.Datatables', true)
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Your QR Cards') }}</div>

                    <div class="card-body">
                        @if(session('success'))
                            <x-adminlte-alert theme="success" title="{{ __('Success') }}" dismissable>
                                {{ session('success') }}
                            </x-adminlte-alert>
                        @endif
                        @if(session('warning'))
                            <x-adminlte-alert theme="warning" title="{{ __('Warning') }}" dismissable>
                                {{ session('warning') }}
                            </x-adminlte-alert>
                        @endif

                        @php
                            /** @var $cards */
                            $heads = [
                                ['label' => __('Name'), 'width' => 20],
                                __('Title'),
                                __('Created'),
                                ['label' => __('Is published'), 'width' => 5],
                                ['label' => __('Actions'), 'width' => 10],
                            ];
                            $config = [
                                'serverSide' => true,
                                'processing' => true,
                                'ajax' => route('qr.datatable'),
                                'ordering' => false,
                                'lengthChange' => false,
                                'searchDelay' => 600,
                                'dom' =>  '<"toolbar">frtip',
                                'language' => [
                                    'url' => asset('vendor/datatables-plugins/i18n/ru.json')
                                ]
                            ];
                        @endphp
                        <x-adminlte-datatable id="qr_table" :heads="$heads" :config="$config"></x-adminlte-datatable>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        const btnEdit = id =>
            `<a href="qr/${id}/edit" class="btn btn-xs btn-default text-primary mx-1" title="{{ __('Edit') }}">
                <i class="fa fa-lg fa-fw fa-pen"></i>
            </a>`
        const iconPublished = '<i class="fa fa-lg fa-fw fa-check text-success"></i>'
        const iconUnpublished = '<i class="fa fa-lg fa-fw fa-times text-danger"></i>'
        const newBtn =
            `<a href="{{ route('qr.create') }}" class="btn btn-sm btn-primary">
                <i class="fa fa-fw fa-plus"></i>&nbsp;{{ __('New QR Card') }}
            </a>`

        $.extend(true, $.fn.dataTable.defaults, {
            'initComplete': () => {
                $("div.toolbar").css('float', 'left')
                $("div.toolbar").html(newBtn)
            },
            'columns': [
                {
                    'data': 'name'
                },
                {
                    'data': 'title'
                },
                {
                    'data': 'created_at',
                    render: function ( created_at, type, row ) {
                        let date = new Date(created_at)
                        return date.toLocaleDateString('ru-RU')
                    }
                },
                {
                    'data': 'published',
                    render: function ( published, type, row ) {
                        return published === true ? iconPublished : iconUnpublished;
                    }
                },
                {
                    'data': 'id',
                    render: function ( id, type, row ) {
                        return btnEdit(id)
                    }
                }
            ]
        });

        $(document).ready(function () {
            let dtable = $('#qr_table').dataTable().api()
            let searchWait = 0
            let searchWaitInterval
            $('.dataTables_filter input')
                .unbind() // Unbind previous default bindings
                .bind('input', function(e) {
                    let item = $(this)
                    searchWait = 0
                    if ($(item).val() === '') {
                        dtable.search('').draw()
                        return
                    }
                    if(!searchWaitInterval) searchWaitInterval = setInterval(function() {
                        if(searchWait >= 1) {
                            clearInterval(searchWaitInterval)
                            searchWaitInterval = ''
                            let searchTerm = $(item).val()
                            if (searchTerm.length >= 3) {
                                dtable.search(searchTerm).draw()
                            }
                            searchWait = 0
                        }
                        searchWait++
                    }, 300)
                })
                .bind('keydown', function(e) {
                    // If the user pressed ENTER, search
                    if (e.keyCode === 13) {
                        dtable.search(this.value).draw()
                    }
                });
        })
    </script>
@endsection

