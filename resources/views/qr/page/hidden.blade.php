<html>
<head>
    <title>{{ __('Whoops!') }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-light d-flex flex-column vh100">
    <div class="hidden_page text-secondary">
        {{ __('Sorry, this page has been hidden by author.') }}
        @if ($email || $phone)
            <br>{{ __('You can contact him') }}
            @if ($email)
                {{ __('by email:') }} <a href="mailto:{{ $email }}" class="text-secondary">{{ $email }}</a>
                @if ($phone) <br>{{ __('or') }} @endif
            @endif
            @if ($phone) {{ __('by phone:') }} <small>{{ $phone }}</small> @endif
        @endif
    </div>
</body>
</html>
