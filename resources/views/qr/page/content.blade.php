<html>
<head>
    <title>{{ $qr->title }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
</head>
<body class="bg-white d-flex flex-column vh100">
    <div class="wrapper flex-grow-1">
        <div class="row justify-content-start justify-content-md-center align-items-center">
            <h1 class="col-auto my-0 ms-2 p-0" style="min-height: 48px; line-height: 48px">{{ $qr->title }}</h1>
        </div>
        <div class="container-fluid" class="container-fluid" style="padding-top: 35px">
            @if($qr->file_links)
                <div>{{ __('Additional files:') }}</div>
                @foreach($qr->file_links ?? [] as $file)
                    <div class="col-12 d-inline-flex flex-row align-items-center mb-1">
                        <a href="{{ route('qr.file', ['card_code' => $qr->card_code, 'file_id' => $file['id']]) }}"
                           target="_blank" class="file_link">
                            <i class="fa fa-download me-1"></i><span>{{ $file['name'] }}</span>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
        @php
        $str = str_replace("\n", "<br>", $qr->body);
        @endphp
        <main class="container-fluid" style="padding-top: 35px">
            {!! $str !!}
        </main>
    </div>
    <footer class="container-fluid bg-dark p-4 pt-2 mt-2">
        <div class="row align-items-center">
            @if($qr->parameters['logo_url'] ?? null)
                <img class="col-auto my-2 ms-2 p-0" src="{{ $qr->parameters['logo_url'] }}" alt="Logo" height="50">
            @endif
            @if(isset($qr->author->settings->parameters['email']) || isset($qr->author->settings->parameters['phone']))
                <div class="col-auto py-2 footer__contacts">
                    <div class="col-12 col-md-auto text-light pe-0">{{ __('Contact us:') }}</div>
                    @if(isset($qr->author->settings->parameters['email']))
                        <div class="col-12 col-md-auto text-secondary footer__email">
                            {{ __('Email:') }} {{ $qr->author->settings->parameters['email'] }}
                        </div>
                    @endif
                    @if(isset($qr->author->settings->parameters['phone']))
                        <div class="col-12 col-md-auto text-secondary footer__email">
                            {{ __('Phone:') }} <small>{{ $qr->author->settings->parameters['phone'] }}</small>
                        </div>
                    @endif
                </div>
            @endif
        </div>
        <div class="row text-secondary border-top border-secondary pt-2 footer__contacts">
            <div class="col-auto">© 2022 — <a href="{{ route('home') }}" class="text-secondary">{{ config('app.name') }}</a></div>
        </div>
    </footer>
    <script type="text/javascript">
        let vh = window.innerHeight * 0.01
        document.documentElement.style.setProperty('--vh', `${vh}px`)
        window.addEventListener('resize', () => {
            let vh = window.innerHeight * 0.01
            document.documentElement.style.setProperty('--vh', `${vh}px`)
        })
    </script>
</body>
</html>
