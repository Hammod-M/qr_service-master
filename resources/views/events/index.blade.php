@extends('adminlte::page')

@section('title', __('Events'))

@section('content_header')
    <h1>{{ __('Events') }}</h1>
@stop

@section('plugins.Datatables', true)
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">{{ __('Events') }}</div>

                    <div class="card-body">
                        @if(session('success'))
                            <x-adminlte-alert theme="success" title="{{ __('Success') }}" dismissable>
                                {{ session('success') }}
                            </x-adminlte-alert>
                        @endif
                        @if(session('warning'))
                            <x-adminlte-alert theme="warning" title="{{ __('Warning') }}" dismissable>
                                {{ session('warning') }}
                            </x-adminlte-alert>
                        @endif

                        @php
                            $heads = [
                                __('Card'),
                                __('Label'),
                                __('Event'),
                                __('Date'),
                                __('IP'),
                            ];
                            $config = [
                                'serverSide' => true,
                                'ordering' => false,
                                'searching' => false,
                                'ajax' => route('events.datatable'),
                                'deferRender' => true,
                                'scrollerCollapse' => true,
                                'scroller' => [
                                    'loadingIndicator' => true,
                                    'displayBuffer' => 3
                                ],
                                'language' => [
                                    'url' => asset('vendor/datatables-plugins/i18n/ru.json')
                                ]
                            ];
                        @endphp
                        <x-adminlte-datatable id="events_table" :heads="$heads" :config="$config"></x-adminlte-datatable>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $.extend(true, $.fn.dataTable.defaults, {
            columns: [
                {
                    data: 'name',
                },
                {
                    data: 'label',
                },
                {
                    data: 'event',
                    render: function (event) {
                        if (event === null) {
                            return '{{ __('Open') }}'
                        }
                        if (event.length > 50) {
                            event = event.slice(0, 50) + '...'
                        }
                        return `{{ __('Download file') }} "${event}"`
                    },
                },
                {
                    data: 'created_at',
                    render: created_at => new Date(created_at).toLocaleString('ru-RU'),
                },
                {
                    data: 'ip',
                },
            ],
            scrollY:  $(window).height() - 350,
        });
    </script>
@endsection
