@extends('auth.layout')

@section('title', __('Home'))

@section('content_header')
    <h1>{{ __('Home') }}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('QR Service') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You can') }} <a href="{{ route('login') }}">{{__('login')}}</a> {{ __('or') }} <a href="{{ route('register') }}">{{__('register')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
