@extends('adminlte::page')

@section('title', __('Settings'))

@section('content_header')
    <h1>{{ __('Settings') }}</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Settings') }}</div>

                <div class="card-body">
                    @if(session('success'))
                        <x-adminlte-alert theme="success" title="{{ __('Success') }}" dismissable>
                            {{ session('success') }}
                        </x-adminlte-alert>
                    @endif

                    @include('settings.form')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
