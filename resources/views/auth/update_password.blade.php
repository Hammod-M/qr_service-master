@extends('auth.layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Update your password') }}</div>
                    <div class="card-body">
                        <form class="d-inline" method="POST" action="{{ route('update_password') }}" class="container">
                            @csrf
                            <x-adminlte-input name="password"
                                              type="password"
                                              fgroup-class="mb-2"
                                              label="{{ __('You just have logged in with temporary password. Please, change it:') }}"
                                              placeholder="{{ __('New password')}}">
                                <x-slot name="bottomSlot">
                                    <small style="color: #555">
                                        {{
                                            trans_choice(
                                                'Min :min characters length',
                                                \App\Http\Requests\UpdatePasswordRequest::MIN_LENGTH,
                                                ['min' => \App\Http\Requests\UpdatePasswordRequest::MIN_LENGTH]
                                            )
                                        }}
                                    </small>
                                </x-slot>
                            </x-adminlte-input>
                            <x-adminlte-button type="submit"
                                               label="{{ __('Save') }}"
                                               theme="primary"></x-adminlte-button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
