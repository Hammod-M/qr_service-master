#!/bin/bash
chown -R www-data:www-data /app/storage/
sudo -E -u www-data cp -R /app/storage_tmpl/* /app/storage/

sudo -E -u www-data php artisan optimize

if [ $TASK_SLOT -eq 1 ]; then
    sudo -E -u www-data php artisan migrate || exit 1
fi

php-fpm

