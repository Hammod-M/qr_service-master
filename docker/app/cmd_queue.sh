#!/bin/sh
chown -R www-data:www-data /app/storage/
sudo -E -u www-data cp -R /app/storage_tmpl/* /app/storage/

sudo -E -u www-data php artisan queue:work --daemon --queue=default --delay=0 --memory=128 --sleep=3 --tries=1
