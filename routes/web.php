<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\QrCard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/update_password', [App\Http\Controllers\Auth\UpdatePasswordController::class, 'show'])
    ->name('update_password');
Route::post('/update_password', [App\Http\Controllers\Auth\UpdatePasswordController::class, 'updatePassword'])
    ->name('update_password_post');

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'user/'], function() {
    Route::resource('settings', App\Http\Controllers\SettingsController::class)->only(['index', 'store']);
    Route::resource('qr', App\Http\Controllers\QrCardController::class)->only(['index', 'create', 'update', 'edit', 'destroy']);
    Route::get('qr/data', [App\Http\Controllers\QrCardController::class, 'data'])->name('qr.datatable');
    Route::get('qr/{qr}/pdf', [App\Http\Controllers\QrCardController::class, 'getPdf'])->name('qr.pdf');
    Route::resource('events', App\Http\Controllers\EventsController::class)->only(['index']);
    Route::get('events/data', [App\Http\Controllers\EventsController::class, 'data'])->name('events.datatable');
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])
    ->name('home');

Route::get('qr/{card_code}', [App\Http\Controllers\QrCardController::class, 'page'])
    ->name('qr.page');
Route::get('qr/{card_code}/file/{file_id}', [App\Http\Controllers\QrCardController::class, 'fileDownload'])
    ->name('qr.file');
