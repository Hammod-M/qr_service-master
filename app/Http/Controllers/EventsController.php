<?php

namespace App\Http\Controllers;

use App\Http\Requests\DatatableRequest;
use App\Models\Event;
use App\Models\QrCard;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        return view('events.index');
    }

    public function data(DatatableRequest $request)
    {
        $validated = $request->validated();
        $query = Event::query()
            ->where('events.user_id', Auth::id())
            ->join('qr_cards', 'qr_cards.id', DB::raw("(info->>'qr')::int"))
            ->orderBy('events.created_at', 'desc')
            ->select(['info', 'qr_cards.name', 'events.created_at']);
        $paginated = $query->paginate($validated['length'], page: ($validated['start'] / $validated['length']) + 1);

        $data = [
            'draw' => $validated['draw'],
            'recordsTotal' => $paginated->total(),
            'recordsFiltered' => $paginated->total(),
            'data' => collect($paginated->items())->map(function($item) {
                /** @var QrCard $qr */
                $qr = QrCard::find($item['info']['qr']);
                $item['event'] = $item['info']['file'] ?? null;
                $item['label'] = $qr->label;
                $item['ip'] = $item['info']['ip'];
                unset($item['info']);
                return $item;
            }),
        ];
        return response()->json($data);
    }
}
