<?php

namespace App\Http\Controllers;


use App\Http\Requests\SettingsRequest;
use App\Models\QrSettings;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    private const LOGO_PATH = 'logo';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $settings = QrSettings::query()->firstOrNew(['user_id' => Auth::id()]);
        return view('settings.edit', ['settings' => $settings]);
    }

    public function store(SettingsRequest $request)
    {
        $validated = $request->validated();
        /** @var QrSettings $settings */
        $settings = QrSettings::query()->firstOrNew(['user_id' => Auth::id()]);
        $settings->user_id = Auth::id();

        $parameters = $settings->parameters;
        if (isset($validated['logo'])) {
            $path = $validated['logo']->storePublicly($this::LOGO_PATH);
            $parameters['logo_path'] = $path;
            $parameters['logo_url'] = Storage::url($path);
        }
        if (isset($validated['leading_text']) || is_null($validated['leading_text'])) {
            $parameters['leading_text'] = $validated['leading_text'];
        }
        if (isset($validated['phone'])) {
            $parameters['phone'] = $validated['phone'] == QrSettings::PHONE_MASK ? null : $validated['phone'];
        }
        $parameters['email'] = $validated['email'] ?? null;
        $parameters['fields'] = $validated['fields'] ?? [];
        $settings->parameters = $parameters;
        $settings->save();

        return redirect()->route('settings.index')->with('success', __('Settings updated successfully.'));
    }
}
