<?php

namespace App\Http\Controllers;

use App\Http\Requests\DatatableRequest;
use App\Http\Requests\QrCardUpdateRequest;
use App\Models\Event;
use App\Models\QrCard;
use App\Models\User;
use App\Services\QrService;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ItemNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QrCardController
{
    private const FILES_PATH = 'qr/files';

    public function index()
    {
        return view('qr.index');
    }

    public function edit(QrCard $qr)
    {
        Gate::allowIf(fn($user) => $qr->user_id == $user->id);
        $qrImage = QrService::CreateQR($qr);
        return view('qr.edit', compact('qr', 'qrImage'));
    }

    public function create()
    {
        $qr = new QrCard();
        $qr->save();
        return redirect()
            ->route('qr.edit', compact('qr'))
            ->with('warning', __('QR Card saved as draft and will be removed in one day!'));
    }

    public function update(QrCardUpdateRequest $request, QrCard $qr)
    {
        /** @var User $user */
        $user = Auth::user();
        $validated = $request->validated();
        $parameters = $qr->parameters;
        $qr->update($request->except('parameters'));
        $parameters['fields'] = collect($validated['parameters']['fields'] ?? [])
            ->filter(fn($parameter) => $parameter != null)
            ->map(fn($item, $key) => [
                'name' => $key,
                'value' => $item
            ])
            ->values()
            ->toArray();
        if ($validated['use_from_settings']['leading_text'] ?? false) {
            $parameters['leading_text'] = $user->settings?->parameters['leading_text'];
        }
        if ($validated['use_from_settings']['logo_url'] ?? false) {
            $parameters['logo_url'] = $user->settings?->parameters['logo_url'];
        }
        $qr->parameters = $parameters;

        Storage::delete(
            collect($qr->file_links)
                ->filter(fn($link) => in_array($link['id'], $validated['delete_files']))
                ->pluck('path')
                ->toArray()
            );

        $qr->file_links = collect($qr->file_links)
            ->filter(fn($link) => !in_array($link['id'], $validated['delete_files']))
            ->toArray();
        if (isset($validated['file'])) {
            foreach($validated['file'] as $file) {
                $path = $file->storePubliclyAs($this::FILES_PATH, $file->getClientOriginalName());
                $id = collect($qr->file_links)->max('id') + 1;
                $qr->file_links = collect($qr->file_links)->add([
                    'path' => $path,
                    'id' => $id,
                    'url' => Storage::url($path),
                    'name' => $file->getClientOriginalName()
                ])->toArray();
            }
        }
        $qr->is_draft = !($qr->name && $qr->title);
        $qr->save();
        if ($qr->is_draft) {
            return redirect()->route('qr.edit', $qr->id)
                ->with('warning', __('QR Card saved as draft and will be removed in one day!'));
        }
        return redirect()->route('qr.edit', $qr->id)->with('success', __('QR Card updated successfully.'));
    }

    public function destroy(QrCard $qr)
    {
        Gate::allowIf(fn($user) => $qr->user_id == $user->id);
        //Storage::delete(collect($qr->file_links)->pluck('path')->toArray());
        $qr->delete();
        return redirect()->route('qr.index')->with('success', __('QR Card deleted successfully.'));
    }

    public function page(Request $request, string $card_code)
    {
        /** @var QrCard $qr */
        $qr = QrCard::query()->where('card_code', $card_code)->firstOrFail();
        if ($qr->published) {
            $event = new Event();
            $event->user_id = $qr->user_id;
            $event->info = [
                'qr' => $qr->id,
                'ip' => $request->ip(),
            ];
            $event->save();
            return view('qr.page.content', compact('qr'));
        }
        return view('qr.page.hidden', [
            'email' => $qr->author->settings->parameters['email'] ?? null,
            'phone' => $qr->author->settings->parameters['phone'] ?? null,
        ]);
    }

    public function fileDownload(Request $request, string $card_code, int $fileId)
    {
        /** @var QrCard $qr */
        $qr = QrCard::query()->where('card_code', $card_code)->firstOrFail();

        try {
            $file = collect($qr->file_links)->firstOrFail('id', $fileId);
        } catch (ItemNotFoundException) {
            throw new NotFoundHttpException();
        }

        $event = new Event();
        $event->user_id = $qr->user_id;
        $event->info = [
            'qr' => $qr->id,
            'file' => $file['name'],
            'ip' => $request->ip(),
        ];
        $event->save();
        return redirect($file['url']);
    }

    public function data(DatatableRequest $request)
    {
        $validated = $request->validated();
        $query = QrCard::query()->where('user_id', Auth::id());
        if (isset($validated['search']['value'])) {
            $query->where('name', 'ilike', $validated['search']['value'].'%');
        }
        $query->orderBy('created_at', 'desc');
        $paginated = $query->paginate($validated['length'], page: ($validated['start'] / $validated['length']) + 1);

        $data = [
            'draw' => $validated['draw'],
            'recordsTotal' => $paginated->total(),
            'recordsFiltered' => $paginated->total(),
            'data' => $paginated->items(),
        ];
        return response()->json($data);
    }

    public function getPdf(QrCard $qr) {
        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);
        $dompdf = new Dompdf($options);
        $qrImage = QrService::CreateQR($qr);
        $html = view('qr.pdf', compact('qr', 'qrImage'))->render();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4');
        $dompdf->render();
        $dompdf->stream($qr->title.'.pdf', ['Attachment' => 0]);
    }
}
