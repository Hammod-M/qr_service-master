<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * @return Application|Factory|View|RedirectResponse
     */
    public function home(): View|Factory|RedirectResponse|Application
    {
        if (Auth::check()) {
            return redirect()->route('qr.index');
        }
        return view('home');
    }
}
