<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'file|mimes:png|max:100',
            'leading_text' => 'string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/|present|nullable|max:50',
            'fields' => 'array',
            'fields.*' => 'string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/|max:32',
            'email' => 'string|email|present|nullable|max:100',
            'phone' => [
                'string',
                'regex:/^(\+7(\s+)?\(?[0-9]{3}\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2})|(\+7 \(___\) ___-__-__)$/',
                'present',
                'nullable',
                'max:32'
            ]
        ];
    }

    public function messages()
    {
        return [
            'fields.*.max' => __('Each parameter must not be greater than :max characters.'),
            'fields.*.regex' => __('Each parameter must only contain simple text.'),
            'fields.*.not_regex' => __('Parameters cannot contain special characters.'),
            'leading_text.regex' => __('The leading text must only contain simple text.'),
        ];
    }
}
