<?php

namespace App\Http\Requests;

use App\Models\QrCard;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class QrCardUpdateRequest extends FormRequest
{
    public const MAX_FILES_COUNT = 3;
    public const MAX_BODY_SIZE = 1024 * 1024;  // 1 Mb
    public const MAX_STRIPPED_BODY_SIZE = 500;
    public const MAX_FILES_SIZE_MB = 20;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->qr->user_id == Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required_if:published,true|nullable|string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/|max:64',
            'title' => 'required_if:published,true|nullable|string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/',
            'label' => 'required_if:published,true|nullable|string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/',
            'body' => [
                'present',
                'nullable',
                'string',
            ],
            'parameters' => 'array:fields',
            'parameters.fields' => 'array',
            'parameters.fields.*' => 'present|nullable|string|regex:/^[\pL\pP\s\d@]+$/u|not_regex:/.*[\'"&].*/|max:32',
            'use_from_settings' => 'array',
            'use_from_settings.*' => 'boolean',
            'published' => 'boolean',
            'delete_files' => 'array',
            'delete_files.*' => 'present|nullable|integer',
            'file' => [
                'array',
                'max:'.self::MAX_FILES_COUNT,
                function ($attribute, array $value, $fail) {
                    /** @var QrCard $qr */
                    $qr = $this->qr;
                    $currentCount = count($qr->file_links ?? []);
                    $deleteCount = collect($this->delete_files)->filter(fn($v) => $v != null)->count();
                    $uploadCount = count($value);
                    if ($currentCount + $uploadCount - $deleteCount > self::MAX_FILES_COUNT) {
                        $fail(__('Maximum total number of files is :max.', ['max' => self::MAX_FILES_COUNT]));
                    }
                }],
            'file.*' => 'required|file|max_mb:'.self::MAX_FILES_SIZE_MB
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => __('The name must only contain simple text.'),
            'name.not_regex' => __('The name cannot contain special characters.'),
            'title.regex' => __('The title must only contain simple text.'),
            'title.not_regex' => __('The title cannot contain special characters.'),
            'label.regex' => __('Event label must only contain simple text.'),
            'label.not_regex' => __('Event label cannot contain special characters.'),
            'parameters.fields.*.max' => __('Parameter value must not be greater than :max characters.'),
            'parameters.fields.*.regex' => __('Parameter value must only contain simple text.'),
            'parameters.fields.*.not_regex' => __('Parameter value cannot contain special characters.'),
            'file.*.max_mb' => __('Each file must not be greater than :max_mb Mb.'),
        ];
    }

    /**
     * Prepare inputs for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'published' => $this->toBoolean($this->published),
            'use_from_settings' => collect($this->use_from_settings)->map(fn($value) => $this->toBoolean($value))->toArray(),
        ]);
    }

    /**
     * Convert to boolean
     *
     * @param $booleable
     * @return boolean
     */
    private function toBoolean($booleable): bool
    {
        return filter_var($booleable, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) ?? false;
    }
}
