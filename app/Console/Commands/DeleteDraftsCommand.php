<?php

namespace App\Console\Commands;

use App\Jobs\DeleteDraftsJob;
use Illuminate\Console\Command;

class DeleteDraftsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marking:delete-drafts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old drafts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DeleteDraftsJob::dispatch();
        return 0;
    }
}
