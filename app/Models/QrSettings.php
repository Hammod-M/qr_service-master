<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $user_id
 * @property User $owner
 * @property array $parameters
 * @property array $fields
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class QrSettings extends Model
{
    use HasFactory;

    public const PHONE_MASK = '+7 (___) ___-__-__';

    protected $fillable = [
        'user_id',
        'parameters',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'parameters' => 'array'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->parameters = [
            'logo_url' => null,
            'leading_text' => null,
            'email' => null,
            'phone' => null,
            'fields' => []
        ];
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getFieldsAttribute(): array
    {
        return $this->parameters['fields'] ?? [];
    }
}
