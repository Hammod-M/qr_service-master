<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $user_id
 * @property User $owner
 * @property array $info
 * @property Carbon $deleted_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'info',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'info' => 'array'
    ];

    public function owner(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
