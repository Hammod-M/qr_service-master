<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property int $user_id
 * @property User $author
 * @property string $name
 * @property string $title
 * @property string $label
 * @property string $card_code
 * @property string $header
 * @property string $body
 * @property array $file_links
 * @property array $parameters
 * @property array $fields
 * @property boolean $published
 * @property boolean $is_draft
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class QrCard extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'name',
        'title',
        'label',
        'card_code',
        'header',
        'body',
        'file_links',
        'parameters',
        'published',
        'is_draft',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'file_links' => 'array',
        'parameters' => 'array',
        'published' => 'boolean',
        'is_draft' => 'boolean',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->card_code = Str::random(8);
        $this->published = false;
        $this->is_draft = true;
        $this->user_id = Auth::id();
        $this->parameters = [
            'logo_url' => Auth::user()->settings->parameters['logo_url'] ?? null,
            'leading_text' => Auth::user()->settings->parameters['leading_text'] ?? null,
        ];
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getFieldsAttribute(): array
    {
        return collect($this->parameters['fields'] ?? [])
            ->mapWithKeys(fn($item, $key) => [$item['name'] => $item['value']])
            ->toArray();
    }
}
