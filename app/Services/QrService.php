<?php

namespace App\Services;

use App\Models\QrCard;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeEnlarge;
use Endroid\QrCode\Writer\PngWriter;

class QrService
{
    /**
     * @param QrCard $qr QrCard object
     * @param int $size QR-code image size
     * @param int $logoSize logo size
     * @param int $margin margin around QR-code
     * @return string data string to insert into an HTML
     */
    public static function CreateQR(QrCard $qr, int $size = 300, int $logoSize = 100, int $margin = 10): string
    {
        $qrCode = Builder::create()
            ->writer(new PngWriter())
            ->writerOptions([])
            ->data(route('qr.page', $qr->card_code, true))
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->size($size)
            ->margin($margin)
            ->roundBlockSizeMode(new RoundBlockSizeModeEnlarge());
        if (isset($qr->parameters['logo_url'])) {
            $qrCode = $qrCode->logoPath($qr->parameters['logo_url'])
                ->logoResizeToHeight($logoSize);
        }
        $qrCode = $qrCode->build();
        return $qrCode->getDataUri();
    }
}
