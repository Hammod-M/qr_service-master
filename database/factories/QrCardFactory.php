<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class QrCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(32),
            'title' => $this->faker->text(32),
            'header' => $this->faker->text(50),
            'card_code' => $this->faker->regexify('[A-Za-z0-9]{8}'),
            'body' => $this->faker->randomHtml(),
            'parameters' => [],
            'published' => $this->faker->boolean(),
            'is_draft' => $this->faker->boolean(),
        ];
    }
}
