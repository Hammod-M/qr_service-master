<?php

namespace Database\Factories;

use App\Models\QrCard;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /** @var QrCard $qr */
        $qr = QrCard::all()->random();
        return [
            'info' => [
                'qr' => $qr->id,
                'file' => $this->faker->randomElement(['', ...collect($qr->file_links)->pluck('name')->toArray()]),
                'ip' => $this->faker->ipv4
            ],
            'created_at' => $this->faker->dateTimeBetween(startDate: '-30 days')
        ];
    }
}
