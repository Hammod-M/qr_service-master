<?php

namespace Database\Seeders;

use App\Models\QrCard;
use Illuminate\Database\Seeder;

class QrCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QrCard::factory(50)->create(['user_id' => 9]);
    }
}
