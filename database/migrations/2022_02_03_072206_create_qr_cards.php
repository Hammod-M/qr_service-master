<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_cards', function (Blueprint $table) {
            $table->id();
            $table->index(['created_at', 'user_id']);
            $table->foreignId('user_id')->references('id')->on('users');
            $table->index('user_id');
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->index(['name', 'user_id']);
            $table->string('card_code')->unique()->index();
            $table->string('header')->nullable();
            $table->text('body')->nullable();
            $table->jsonb('file_links')->nullable();
            $table->jsonb('parameters')->nullable();
            $table->boolean('is_unpublished')->default(true);
            $table->boolean('is_draft')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_cards');
    }
}
