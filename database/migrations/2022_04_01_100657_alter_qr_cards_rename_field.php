<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterQrCardsRenameField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qr_cards', function (Blueprint $table) {
            $table->renameColumn('is_unpublished', 'published');
        });
        DB::statement('UPDATE qr_cards SET published = not published;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qr_cards', function (Blueprint $table) {
            $table->renameColumn('published', 'is_unpublished');
        });
    }
}
