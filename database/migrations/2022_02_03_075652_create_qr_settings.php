<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_settings', function (Blueprint $table) {
            $table->id();
            $table->index(['created_at', 'user_id']);
            $table->foreignId('user_id')->references('id')->on('users');
            $table->index('user_id');
            $table->string('logo_url')->nullable();
            $table->string('header')->nullable();
            $table->jsonb('parameters')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_settings');
    }
}
